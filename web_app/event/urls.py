from django.urls import path
from .views import CreateEvent, ListEvent, EventDetails, DeleteEvent, UpdateEvent

app_name = 'event'

urlpatterns = [
    path('create/', CreateEvent.as_view(), name='event-create'),
    path('list/', ListEvent.as_view(), name='event-list'),
    path('details/<uuid:pk>/', EventDetails.as_view(), name='event-details'),
    path('delete/<uuid:pk>/', DeleteEvent.as_view(), name='event-delete'),
    path('update/<pk>/', UpdateEvent.as_view(), name='event-update'),
]
