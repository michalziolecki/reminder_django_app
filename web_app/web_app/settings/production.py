from .base import *

ALLOWED_HOSTS = ['*']

# sending emails
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = config('EMAIL_ACCOUNT', cast=str)
EMAIL_HOST_PASSWORD = config('EMAIL_PASSWORD', cast=str)

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# logging
LOGGER_NAME = 'ReminderLogger'
U_LOGFILE_SIZE = 1 * 1024 * 1024
U_LOGFILE_COUNT = 2
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{name} - {asctime} - {module} - {levelname} {message}',
            'style': '{',
        },
        'basic': {
            'format': '{name} - {levelname} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'file': {
            'level': config('LOG_LEVEL', cast=str),
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs', 'develop_logs.log'),
            'maxBytes': U_LOGFILE_SIZE,
            'backupCount': U_LOGFILE_COUNT,
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'ReminderLogger': {
            'handlers': ['file'],
            'level': config('LOG_LEVEL', cast=str),
            'propagate': True,
        },
    },
}
