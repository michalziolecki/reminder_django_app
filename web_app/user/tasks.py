# Create your tasks here
from __future__ import absolute_import, unicode_literals

from celery import shared_task
from .utils import *


@shared_task
def task_send_confirmation_email(user_pk, token):
    user = User.objects.filter(pk=user_pk).get()
    token_entity = ConfirmationToken.objects.filter(token=token).get()
    send_confirmation_email(user, token_entity)
