# docker file for web app - reminder

FROM python:3.6

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /Remider_web
WORKDIR /Remider_web

COPY requirements /Remider_web/
RUN pip install -r /Remider_web/requirements

#RUN mkdir /Remider_web/
COPY web_app/ /Remider_web/web_app/

# listing files after copying
RUN ls -la /Remider_web/web_app/*

# tmp local run method
RUN python /Remider_web/web_app/manage.py migrate
ENTRYPOINT ["python"]
CMD ["/Remider_web/web_app/manage.py", "runserver", "0.0.0.0:8000"]